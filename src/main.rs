use unicorn_client::{client::UnicornClient, error::UnicornClientError};
use unicorn_config;

mod logger;

fn main() -> Result<(), UnicornClientError> {
    let cfg = unicorn_config::config::Configuration::new();
    logger::UnicornLogger::init(&cfg.preferences);
    let mut client = UnicornClient::new(cfg)?;
    client.run()?;
    Ok(())
}
