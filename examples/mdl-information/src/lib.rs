use unicorn_callable::module::UnicornModule;
use unicorn_callable::{define_module, UnicornCommand, UnicornEvent};

use crate::details::{channel::ChannelInfoCommand, guild::GuildInfoCommand, role::RoleInfoCommand, user::UserInfoCommand};
use crate::snowflakes::{channel::ChannelIDCommand, guild::GuildIDCommand, role::RoleIDCommand, user::UserIDCommand};

mod details;
mod snowflakes;
pub mod utils;

#[derive(Default)]
pub struct FoldingAtHomeModule;

impl UnicornModule for FoldingAtHomeModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![
            // Details
            UserInfoCommand::boxed(),
            GuildInfoCommand::boxed(),
            ChannelInfoCommand::boxed(),
            RoleInfoCommand::boxed(),
            // Snowflakes
            UserIDCommand::boxed(),
            GuildIDCommand::boxed(),
            ChannelIDCommand::boxed(),
            RoleIDCommand::boxed(),
        ]
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        Vec::new()
    }

    fn name(&self) -> &str {
        "Information"
    }
}

define_module!(FoldingAtHomeModule, Default::default);
