use std::collections::HashMap;

use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;
use serenity::model::guild::Member;
use serenity::model::id::UserId;
use serenity::model::user::User;
use serenity::utils::Colour;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_callable::UnicornCommand;
use unicorn_embed::UnicornEmbed;
use unicorn_utility::user::Avatar;

pub const DATE_FMT: &str = "%a, %d. %h. %Y";

#[derive(Default)]
pub struct UserInfoCommand;

impl UserInfoCommand {
    fn user_to_member(pld: &CommandPayload, user: User) -> Option<Member> {
        if let Some(guild) = pld.msg.guild(&pld.ctx.cache) {
            if let Ok(member) = guild.read().member(pld.ctx.http(), user.id) {
                Some(member)
            } else {
                None
            }
        } else {
            None
        }
    }

    fn best_member(members: HashMap<UserId, Member>, lookup: &str) -> Option<Member> {
        let mut best = None;
        for (_, member) in &members {
            let user = &member.user.read().clone();
            let full = format!("{}#{:0>4}", &user.name, &user.discriminator);
            if full.to_lowercase() == lookup.to_lowercase() {
                best = Some(member.clone());
            } else if user.name.to_lowercase() == lookup.to_lowercase() {
                best = Some(member.clone());
            } else if let Some(nick) = &member.nick {
                if nick.to_lowercase() == lookup.to_lowercase() {
                    best = Some(member.clone());
                }
            }
            if best.is_some() {
                break;
            }
        }
        if best.is_none() {
            for (_, member) in &members {
                let user = &member.user.read().clone();
                if user.name.to_lowercase().contains(&lookup.to_lowercase()) {
                    best = Some(member.clone());
                } else if let Some(nick) = &member.nick {
                    if nick.to_lowercase().contains(&lookup.to_lowercase()) {
                        best = Some(member.clone());
                    }
                }
                if best.is_some() {
                    break;
                }
            }
        }
        best
    }

    fn search_local(pld: &CommandPayload, lookup: &str) -> Option<Member> {
        if let Some(guild) = pld.msg.guild(&pld.ctx.cache) {
            if let Ok(nl) = lookup.parse::<u64>() {
                if let Ok(memb) = guild.read().member(pld.ctx.http(), nl) {
                    Some(memb)
                } else {
                    None
                }
            } else {
                Self::best_member(guild.read().members.clone(), lookup)
            }
        } else {
            None
        }
    }

    fn search_global(pld: &CommandPayload, lookup: &str) -> Option<Member> {
        let mut best = None;
        let lookup_int = lookup.parse::<u64>();
        let guilds = pld.ctx.cache.read().guilds.clone();
        for (gid, _) in guilds {
            if let Some(guild) = pld.ctx.cache.read().guild(gid) {
                if let Ok(nl) = lookup_int {
                    if let Ok(memb) = guild.read().member(pld.ctx.http(), nl) {
                        best = Some(memb);
                    }
                }
                if best.is_none() {
                    let members = guild.read().members.clone();
                    best = Self::best_member(members, lookup);
                    if best.is_some() {
                        break;
                    }
                }
            }
        }
        best
    }

    pub fn search(pld: &CommandPayload, lookup: &str) -> Option<Member> {
        if let Some(member) = Self::search_local(pld, lookup) {
            Some(member)
        } else {
            if pld.cfg.discord.owners.contains(&pld.msg.author.id.0) {
                Self::search_global(pld, lookup)
            } else {
                None
            }
        }
    }

    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

impl UnicornCommand for UserInfoCommand {
    fn command_name(&self) -> &str {
        "userinformation"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["userinfo", "uinfo"]
    }

    fn example(&self) -> &str {
        "@target"
    }

    fn description(&self) -> &str {
        "Shows information and data on the mentioned user. \
        If no user is mentioned, it will show data for the author."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("target", false).allows_spaces()]
    }

    fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let target = if pld.msg.mentions.is_empty() {
            if pld.args.has_argument("target") {
                Self::search(pld, pld.args.get("target"))
            } else {
                Self::user_to_member(pld, pld.msg.author.clone())
            }
        } else {
            Self::user_to_member(pld, pld.msg.mentions[0].clone())
        };
        let mut response = if let Some(target) = target {
            let mut msg = CreateMessage::default();
            let user = target.user.read().clone();
            msg.embed(|e| {
                e.color(target.colour(&pld.ctx.cache).unwrap_or(Colour::from(0)));
                e.author(|a| {
                    let avatar = Avatar::url(target.user.read().clone());
                    a.name(format!("{}'s Information", target.display_name()));
                    a.icon_url(&avatar);
                    a.url(avatar);
                    a
                });
                e.field(
                    "User Info",
                    format!(
                        "Username: **{}**#{:0>4}\n\
                        ID: **{}**\n\
                        Bot User: **{}**\n\
                        Created: **{}**",
                        &user.name,
                        user.discriminator,
                        user.id.0,
                        if user.bot { "True" } else { "False" },
                        user.created_at().format(DATE_FMT)
                    ),
                    true,
                );
                e.field(
                    "Member Info",
                    format!(
                        "Nickname: **{}**\n\
                        Color: **#{}**\n\
                        Top Role: **{}**\n\
                        Joined: **{}**",
                        if let Some(nick) = &target.nick { nick } else { &user.name },
                        if let Some(color) = target.colour(&pld.ctx.cache) {
                            color.hex()
                        } else {
                            "000000".to_owned()
                        },
                        if let Some(role) = target.highest_role_info(&pld.ctx.cache) {
                            if let Some(role) = role.0.to_role_cached(&pld.ctx.cache) {
                                role.name
                            } else {
                                "everyone".to_owned()
                            }
                        } else {
                            "everyone".to_owned()
                        },
                        if let Some(jd) = target.joined_at {
                            jd.format(DATE_FMT).to_string()
                        } else {
                            "Unknown".to_owned()
                        }
                    ),
                    true,
                );
                e.footer(|f| {
                    f.text(format!(
                        "Use the {}avatar command to see the user's avatar, or click the title.",
                        pld.get_prefix()
                    ));
                    f
                });
                e
            });
            msg
        } else {
            UnicornEmbed::not_found("User not found.")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response)?;
        Ok(())
    }
}
