use serenity::model::channel::ChannelType;
use serenity::model::guild::Guild;
use serenity::model::id::ChannelId;
use unicorn_callable::payload::CommandPayload;

pub struct ChannelUtils;

impl ChannelUtils {
    pub fn search_guild(guild: Guild, lookup: &str) -> Option<ChannelId> {
        let mut best = None;
        if let Ok(looknum) = lookup.parse::<u64>() {
            if let Some(chn) = guild.channels.get(&ChannelId(looknum)) {
                best = Some(chn.read().clone().id);
            }
        } else {
            for (cid, channel) in &guild.channels {
                let channel = channel.read().clone();
                match channel.kind {
                    ChannelType::Text | ChannelType::Voice => {
                        let lower_match = channel.name.to_lowercase() == lookup.to_lowercase();
                        let pound_match = channel.name.to_lowercase() == format!("#{}", lookup.to_lowercase());
                        if lower_match || pound_match {
                            best = Some(cid.clone());
                            break;
                        }
                    }
                    _ => {}
                }
            }
            if best.is_none() {
                for (cid, channel) in &guild.channels {
                    let channel = channel.read().clone();
                    match channel.kind {
                        ChannelType::Text | ChannelType::Voice => {
                            let lower_cont = channel.name.to_lowercase().contains(&lookup.to_lowercase());
                            let pound_cont = channel.name.to_lowercase().contains(&format!("#{}", lookup.to_lowercase()));
                            if lower_cont || pound_cont {
                                best = Some(cid.clone());
                                break;
                            }
                        }
                        _ => {}
                    }
                }
            }
        }
        best
    }

    pub fn search_local(pld: &CommandPayload, lookup: &str) -> Option<ChannelId> {
        let mut best = None;
        if let Some(guild) = pld.msg.guild(&pld.ctx.cache) {
            let guild = guild.read().clone();
            best = Self::search_guild(guild, lookup)
        }
        best
    }

    pub fn mentioned_channels(pld: &CommandPayload) -> Vec<ChannelId> {
        let is_owner = pld.cfg.discord.owners.contains(&pld.msg.author.id.0);
        let mut mentions = Vec::<ChannelId>::new();
        if let Some(gld) = pld.msg.guild(&pld.ctx.cache) {
            let guild = gld.read().clone();
            for arg in &pld.args.raw() {
                if arg.starts_with("<#") && arg.ends_with(">") {
                    if let Ok(cid) = arg.trim_start_matches("<#").trim_end_matches(">").parse::<u64>() {
                        let channel_id = ChannelId(cid);
                        if guild.channels.contains_key(&channel_id) || is_owner {
                            mentions.push(channel_id);
                        }
                    }
                }
            }
        }
        mentions
    }
}
