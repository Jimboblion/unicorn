use std::any::Any;

use log::info;
use serde::Deserialize;

use unicorn_config::common::ConfigLoader;

#[derive(Debug, Clone, Deserialize)]
pub struct TimezoneConfig {
    pub timezone: String,
    pub offset: i32,
}

impl ConfigLoader for TimezoneConfig {
    fn file_name() -> String {
        "timezone".to_owned()
    }

    fn default() -> Self {
        Self {
            timezone: Self::default_timezone(),
            offset: Self::default_offset(),
        }
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn describe(&self) {
        info!("Timezone: {}", &self.timezone);
        info!("Offset: {}", &self.offset);
    }
}

impl TimezoneConfig {
    pub fn default_timezone() -> String {
        "UTC".to_owned()
    }

    pub fn default_offset() -> i32 {
        0
    }
}
