use serenity::http::CacheHttp;
use serenity::model::user::OnlineStatus;

use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_callable::UnicornCommand;

const SIGMA_IMAGE: &str = "https://i.imgur.com/mGyqMe1.png";
const SIGMA_TITLE: &str = "Apex Sigma: Statistics";
const SUPPORT_URL: &str = "https://discordapp.com/invite/aEUCHwX";

pub struct StatsCommand;

impl StatsCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

impl UnicornCommand for StatsCommand {
    fn command_name(&self) -> &str {
        "stats"
    }

    fn category(&self) -> &str {
        "utility"
    }

    fn description(&self) -> &str {
        "Displays the current time in UTC."
    }

    fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let cache = pld.ctx.cache.read().clone();
        let guild_count = (&cache.guilds).len();
        let mut role_count = 0u64;
        let mut user_count = 0u64;
        let channel_count = cache.channels.len();
        for (_guild_id, guild) in &cache.guilds {
            let guild = guild.read().clone();
            role_count += guild.roles.len() as u64;
            user_count += guild.member_count;
        }
        let mut online_count = 0u64;
        let mut away_count = 0u64;
        let mut busy_count = 0u64;
        let mut offline_count = 0u64;
        for (user_id, _user) in &cache.users {
            match cache.presences.get(&user_id) {
                Some(presence) => match presence.status {
                    OnlineStatus::Idle => away_count += 1,
                    OnlineStatus::DoNotDisturb => busy_count += 1,
                    OnlineStatus::Offline | OnlineStatus::Invisible => offline_count += 1,
                    _ => online_count += 1,
                },
                None => online_count += 1,
            };
        }
        let _ = pld.msg.channel_id.send_message(pld.ctx.http(), |m| {
            m.embed(|e| {
                e.color(1_797_983);
                e.author(|a| {
                    a.name(SIGMA_TITLE);
                    a.icon_url(SIGMA_IMAGE);
                    a.url(SUPPORT_URL);
                    a
                });
                e.field(
                    "Population",
                    format!(
                        "Guilds: **{}**\nChannels: **{}**\nRoles: **{}**\nUsers: **{}**",
                        guild_count, channel_count, role_count, user_count
                    ),
                    true,
                );
                e.field(
                    "User Status",
                    format!(
                        "Online: **{}**\nAway: **{}**\nBusy: **{}**\nOffline: **{}**",
                        online_count, away_count, busy_count, offline_count
                    ),
                    true,
                );
                e
            });
            m
        });
        Ok(())
    }
}
