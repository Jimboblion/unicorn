pub trait GameBoard<Board, Move> {
    fn gameover(board: &Board) -> bool;
    fn score_board(board: &Board) -> f64;
    fn possible_moves(board: &Board) -> Vec<Move>;
    fn apply_move(board: &Board, player_move:Move) -> Board;
    fn winner(board: &Board) -> Option<usize>;  
    fn copy(board: &Board) -> Board;    
}