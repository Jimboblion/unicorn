use rand::Rng;
use serde::{Deserialize, Serialize};

use unicorn_utility::token::UnicornToken;

use crate::professions::item::Item;

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
pub struct InventoryItem {
    pub item_id: String,
    pub file_id: String,
    pub quality: i32,
    pub transferred: bool,
}

impl InventoryItem {
    pub fn create_from_item(item: &Item) -> Self {
        let mut rng = rand::thread_rng();
        let quality = rng.gen_range(0, 7);
        Self {
            item_id: UnicornToken::hex(16),
            file_id: item.file_id.clone(),
            quality: quality,
            transferred: false,
        }
    }
}

impl PartialEq for InventoryItem {
    fn eq(&self, other: &Self) -> bool {
        self.item_id == other.item_id
    }
}
