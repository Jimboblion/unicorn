use serde::{Deserialize, Serialize};

use crate::named::Named;

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
pub struct YamlItem {
    pub name: String,
    #[serde(rename(deserialize = "type", serialize = "type"))]
    pub item_type: String,
    pub description: String,
    pub file_id: String,
    pub rarity: Option<i32>,
}

impl Named for YamlItem {
    fn get_name(&self) -> String {
        self.name.clone()
    }
}

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
pub struct YamlRecipe {
    pub name: String,
    #[serde(rename(deserialize = "type", serialize = "type"))]
    pub recipe_type: String,
    pub description: String,
    pub file_id: String,
    pub ingredients: Vec<String>,
}

impl Named for YamlRecipe {
    fn get_name(&self) -> String {
        self.name.clone()
    }
}
