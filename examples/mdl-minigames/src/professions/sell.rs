use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;
use serenity::model::channel::ReactionType;
use serenity::model::id::UserId;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::dialogue::Dialogue;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::{CommandPayload, ReactionPayload};
use unicorn_callable::reaction::UnicornReaction;
use unicorn_config::config::Configuration;
use unicorn_database::DatabaseHandler;
use unicorn_embed::UnicornEmbed;
use unicorn_resource::origin::ResourceOrigin;
use unicorn_utility::string::UnicornString;
use unicorn_utility::user::Avatar;

use crate::professions::db::DBCollection;
use crate::professions::inventory::Inventory;
use crate::professions::inventory_item::InventoryItem;
use crate::professions::item::Item;

pub struct SellCommand {
    confirm_dialog: Dialogue,
}

impl SellCommand {
    fn new() -> Self {
        let mut confirm_dialog = Dialogue::ok_cancel("sell");
        confirm_dialog.title = Some(Box::new(|pld| SellCommand::sell_all_question(pld)));
        confirm_dialog.ok_callback = Some(Box::new(|pld| SellCommand::sell_all_confirmed(pld)));
        confirm_dialog.cancel_callback = Some(Box::new(|pld| SellCommand::sell_all_cancelled(pld)));
        Self { confirm_dialog }
    }

    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(SellCommand::new())
    }

    pub fn rboxed() -> Box<dyn UnicornReaction> {
        Box::new(SellCommand::new())
    }

    fn sell_all_question(pld: &mut CommandPayload) -> String {
        let mut count = 0;
        let mut value = 0;
        let inventory = Inventory::get_for_player(&pld.db, u64::from(pld.msg.author.id));
        for item in inventory.items {
            if let Some(actual_item) = Item::get_by_id(&pld.db, item.file_id.as_str()) {
                value += actual_item.value;
                count += 1;
            }
        }
        let question = format!(
            "❔ Are you sure you want to sell {} {} worth {} {}?",
            count,
            if count == 1 { "item" } else { "items" },
            value,
            pld.cfg.preferences.currency.common.name.single
        );
        question
    }

    fn sell_all(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        self.confirm_dialog.show_dialog(pld)
    }

    fn sell_all_cancelled(pld: &ReactionPayload) -> Result<bool, CallableError> {
        let mut response = UnicornEmbed::small_embed("❌", 0xBE1931, "Item sale canceled.");
        pld.reaction.channel_id.send_message(pld.ctx.http(), |_| &mut response)?;
        Ok(true)
    }

    fn sell_all_confirmed(pld: &ReactionPayload) -> Result<bool, CallableError> {
        let mut count = 0;
        let mut value = 0;
        let inventory = Inventory::get_for_player(&pld.db, u64::from(pld.reaction.user_id));
        let mut response = CreateMessage::default();
        for item in inventory.items {
            if let Some(actual_item) = Item::get_by_id(&pld.db, item.file_id.as_str()) {
                value += actual_item.value;
                count += 1;
                Inventory::del_item(&pld.db, u64::from(pld.reaction.user_id), item);
            }
        }
        pld.db.add_resource(
            "currency",
            u64::from(pld.reaction.user_id),
            value as i64,
            "sell",
            ResourceOrigin::from(pld.reaction.user_id),
            true,
        );
        if let Ok(author) = pld.reaction.user(pld.ctx.http()) {
            response.embed(|e| {
                e.color(0xF9F9F9);
                e.title(format!(
                    "💶 You sold {} {} for {} {}.",
                    count,
                    if count == 1 { "item" } else { "items" },
                    value,
                    pld.cfg.preferences.currency.common.name.single
                ));
                e.author(|a| {
                    a.name(author.name.clone());
                    a.icon_url(Avatar::url(author.clone()));
                    a
                });
                e
            });
            pld.reaction.channel_id.send_message(pld.ctx.http(), |_| &mut response)?;
        }
        Ok(true)
    }

    fn sell_duplicates<'a>(&self, db: &DatabaseHandler, cfg: &Configuration, user_id: UserId) -> CreateMessage<'a> {
        let mut count = 0;
        let mut value = 0;
        let mut existing_ids = std::collections::HashSet::new();
        let inventory = Inventory::get_for_player(db, u64::from(user_id));
        for item in inventory.items {
            if existing_ids.contains(&item.file_id) {
                if let Some(actual_item) = Item::get_by_id(db, item.file_id.as_str()) {
                    value += actual_item.value;
                    count += 1;
                    Inventory::del_item(db, u64::from(user_id), item);
                }
            } else {
                existing_ids.insert(item.file_id);
            }
        }
        if count > 0 {
            let item_text = if count > 1 { format!("{} items", count) } else { format!("an item") };
            db.add_resource("currency", u64::from(user_id), value as i64, "sell", ResourceOrigin::from(user_id), true);
            UnicornEmbed::small_embed(
                "💸",
                0xc6e4b5,
                format!("You sold {} for {} {}.", item_text, value, cfg.preferences.currency.common.name.single),
            )
        } else {
            UnicornEmbed::not_found("You don't have any duplicates in your inventory.")
        }
    }

    fn sell_multiple_items<'a>(&self, db: &DatabaseHandler, cfg: &Configuration, user_id: UserId, qty: u16, file_id: &str) -> CreateMessage<'a> {
        let mut count = 0;
        let mut value = 0;

        if let Some(actual_item) = Item::get_by_id(db, file_id) {
            while count < qty {
                if let Some(inventory_item) = Inventory::get_item_by_file_id(db, u64::from(user_id), file_id) {
                    Inventory::del_item(db, u64::from(user_id), inventory_item);
                    value += actual_item.value;
                    count += 1;
                } else {
                    break;
                }
            }

            if count > 0 {
                db.add_resource("currency", u64::from(user_id), value as i64, "sell", ResourceOrigin::from(user_id), true);
                UnicornEmbed::small_embed(
                    "💸",
                    0xc6e4b5,
                    format!(
                        "You sold {} {} for {} {}.",
                        qty, actual_item.name, value, cfg.preferences.currency.common.name.single
                    ),
                )
            } else {
                UnicornEmbed::not_found(format!("You don't have any {} in your inventory.", file_id))
            }
        } else {
            UnicornEmbed::not_found(format!("What is {} {} ", UnicornString::connector(file_id), file_id))
        }
    }

    fn sell_item<'a>(&self, db: &DatabaseHandler, cfg: &Configuration, user_id: UserId, item: InventoryItem) -> CreateMessage<'a> {
        Inventory::del_item(db, u64::from(user_id), item.clone());
        if let Some(actual_item) = Item::get_by_id(db, item.file_id.as_str()) {
            let value = actual_item.value;
            db.add_resource("currency", u64::from(user_id), value as i64, "sell", ResourceOrigin::from(user_id), true);
            UnicornEmbed::small_embed(
                "💸",
                0xc6e4b5,
                format!(
                    "You sold {} {} for {} {}.",
                    UnicornString::connector(actual_item.name.as_str()),
                    actual_item.name,
                    value,
                    cfg.preferences.currency.common.name.single
                ),
            )
        } else {
            UnicornEmbed::error(format!("The item {} is in your inventory, but no longer part of the game", item.file_id))
        }
    }
}

impl UnicornCommand for SellCommand {
    fn command_name(&self) -> &str {
        "sell"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn example(&self) -> &str {
        "Copula"
    }

    fn description(&self) -> &str {
        "Sells an item from your inventory. Input \"all\" instead of the item name \
        to sell your entire inventory. Input \"duplicates\" instead of the item name 
        to sell all duplicate items. Put a number before the item name to sell that \
        many of the item."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![
            CommandArgument::new("quantity", true).available_when(|s| s.parse::<u16>().is_ok()),
            CommandArgument::new("item", true).group("thing"),
            CommandArgument::new("duplicates", true).is_flag().group("thing"),
            CommandArgument::new("all", true).is_flag().group("thing"),
        ]
    }

    fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        if pld.args.has_argument("all") {
            self.sell_all(pld)?;
        } else {
            let qty = pld.args.get_or_default("quantity", 1u16);
            let mut response = if pld.args.satisfied() {
                if pld.args.has_argument("duplicates") {
                    self.sell_duplicates(&pld.db, &pld.cfg, pld.msg.author.id)
                } else if qty > 1 {
                    self.sell_multiple_items(&pld.db, &pld.cfg, pld.msg.author.id, qty, pld.args.get("item"))
                } else {
                    match Inventory::get_item_by_file_id(&pld.db, u64::from(pld.msg.author.id), pld.args.get("item")) {
                        Some(item) => self.sell_item(&pld.db, &pld.cfg, pld.msg.author.id, item),
                        None => UnicornEmbed::not_found(format!("Couldn't find {} in your inventory", pld.args.get("item"))),
                    }
                }
            } else if pld.args.has_argument("quantity") {
                UnicornEmbed::not_found(format!("Sell {} of what?", pld.args.get("quantity")))
            } else {
                UnicornEmbed::not_found("Sell what?")
            };
            pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response)?;
        }
        Ok(())
    }
}

impl UnicornReaction for SellCommand {
    fn execute(&self, pld: &mut ReactionPayload) -> Result<(), CallableError> {
        let mut result = Ok(());
        if let ReactionType::Unicode(emoji) = pld.reaction.emoji.clone() {
            if let Ok(message) = pld.reaction.message(pld.ctx.http()) {
                if let Ok(user) = pld.reaction.user(pld.ctx.http()) {
                    match emoji.as_ref() {
                        "💸" => {
                            for embed in message.embeds {
                                if let Some(author) = embed.author {
                                    if author.name == user.name {
                                        if let Some(footer) = embed.footer {
                                            if footer.text.starts_with("item id: ") && (footer.text.len() > 9) {
                                                let item_id = footer.text.split_at(9).1;
                                                if let Some(item) = Inventory::get_item_by_id(&pld.db, u64::from(pld.reaction.user_id), item_id) {
                                                    let mut response = self.sell_item(&pld.db, &pld.cfg, user.id, item);
                                                    pld.reaction.channel_id.send_message(pld.ctx.http(), |_| &mut response)?;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        _ => result = self.confirm_dialog.check_reaction(pld),
                    }
                }
            }
        }
        result
    }

    fn emojis(&self) -> Option<Vec<&str>> {
        Some(vec![vec!["💸"], self.confirm_dialog.options()].concat())
    }
}
