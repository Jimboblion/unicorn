use unicorn_callable::UnicornCommand;

mod coinflip;

pub struct RandomCommands;

impl RandomCommands {
    pub fn commands() -> Vec<Box<dyn UnicornCommand>> {
        vec![coinflip::CoinFlipCommand::boxed()]
    }
}
