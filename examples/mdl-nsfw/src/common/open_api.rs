use log::debug;
use rand::Rng;
use serde::{Deserialize, Serialize};

use unicorn_callable::argument::CommandArguments;
use unicorn_callable::payload::CommandPayload;

use crate::common::error::NSFWError;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct OpenAPIEntry {
    pub model: Option<String>,
    pub preview: String,
    pub id: u64,
    pub rank: u64,
    pub author: Option<String>,
}

impl OpenAPIEntry {
    pub fn new(url: &str) -> Result<Option<Self>, NSFWError> {
        let response = reqwest::blocking::get(url)?;
        let body = response.text()?;
        Self::from_body(body)
    }

    pub fn from_body(body: String) -> Result<Option<Self>, NSFWError> {
        let oar: Vec<Self> = serde_json::from_str(&body)?;
        if oar.is_empty() {
            Ok(None)
        } else {
            Ok(Some(oar[0].clone()))
        }
    }

    pub fn to_body(&self) -> Result<String, serde_json::Error> {
        serde_json::to_string(&vec![self])
    }

    pub fn from_cache(pld: &mut CommandPayload, key: String) -> Result<Option<OpenAPIEntry>, NSFWError> {
        if let Some(cached) = pld.cache.get(key) {
            OpenAPIEntry::from_body(cached)
        } else {
            Ok(None)
        }
    }

    pub fn save_cache(&self, pld: &mut CommandPayload, key: String) {
        match self.to_body() {
            Ok(body) => {
                pld.cache.set(key, body);
            }
            Err(why) => {
                debug!("Failed getting entry body for caching: {}", why);
            }
        }
    }

    pub fn get_num(args: &CommandArguments, top: u64) -> Option<u64> {
        if !args.has_argument("index") {
            let mut rng = rand::thread_rng();
            Some(rng.gen_range(1u64, top))
        } else {
            let lookup = args.get("index");
            if let Ok(num) = lookup.parse::<u64>() {
                Some(num)
            } else {
                None
            }
        }
    }

    pub fn get_model_name(&self) -> String {
        match &self.model {
            Some(mdl) => mdl,
            None => "Unknown",
        }
        .to_owned()
    }

    pub fn get_image_url(&self, base: &str) -> String {
        format!("{}/{}", base, self.preview)
    }
}
