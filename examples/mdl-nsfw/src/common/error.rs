use std::fmt::Display;

use serde::export::fmt::Error;
use serde::export::Formatter;

#[derive(Debug)]
pub enum NSFWError {
    IOError(std::io::Error),
    ReqwestError(reqwest::Error),
    SerdeJSONError(serde_json::Error),
    SerdeXMLError(serde_xml_rs::Error),
}

impl Display for NSFWError {
    //noinspection RsUnresolvedReference
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        match self {
            NSFWError::IOError(err) => err.fmt(f),
            NSFWError::ReqwestError(err) => err.fmt(f),
            NSFWError::SerdeJSONError(err) => err.fmt(f),
            NSFWError::SerdeXMLError(err) => err.fmt(f),
        }
    }
}

impl From<std::io::Error> for NSFWError {
    fn from(err: std::io::Error) -> Self {
        NSFWError::IOError(err)
    }
}

impl From<reqwest::Error> for NSFWError {
    fn from(err: reqwest::Error) -> Self {
        NSFWError::ReqwestError(err)
    }
}

impl From<serde_json::Error> for NSFWError {
    fn from(err: serde_json::Error) -> Self {
        NSFWError::SerdeJSONError(err)
    }
}

impl From<serde_xml_rs::Error> for NSFWError {
    fn from(err: serde_xml_rs::Error) -> Self {
        NSFWError::SerdeXMLError(err)
    }
}
