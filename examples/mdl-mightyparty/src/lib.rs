use unicorn_callable::module::UnicornModule;
use unicorn_callable::{define_module, UnicornCommand, UnicornEvent};

use crate::register::RegisterCommand;
use crate::showcard::ShowCardCommand;
use crate::worldchat::WorldChatCommand;

mod mightyparty;
mod register;
mod showcard;
mod worldchat;

#[derive(Default)]
pub struct MightyPartyModule;

impl UnicornModule for MightyPartyModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![ShowCardCommand::boxed(), WorldChatCommand::boxed(), RegisterCommand::boxed()]
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        Vec::new()
    }

    fn name(&self) -> &str {
        "Mighty Party"
    }
}

define_module!(MightyPartyModule, Default::default);
