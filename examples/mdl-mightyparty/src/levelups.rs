use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;
use log::info;

use crate::mightyparty::client::MightyPartyClient;

#[derive(Default)]
pub struct LevelUpsCommand;

impl LevelUpsCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

impl UnicornCommand for LevelUpsCommand {
    fn command_name(&self) -> &str {
        "mp-levelups"
    }

    fn category(&self) -> &str {
        "mighty party"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["mplu"]
    }

    fn description(&self) -> &str {
        "Counts the total number of available level ups"
    }

    fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let client = MightyPartyClient::new();
        
        //Find player in playerDB
        let player = client.player_from_user(pld.db, u64::pld.msg.author.id);
        let response = 
        match player {
            None => UnicornEmbed::error("Please register before using this command"),
            Some(might_party_id) => {
                let player_info = client.get_player_info(might_party_id);
                //profile/get_player_info
                //read army bit
                //mix with monsters
                UnicornEmbed::ok("Player Found")
            }
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response)?;
        Ok(())
    }
}
