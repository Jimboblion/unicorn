use chrono::{DateTime, Utc};
use serde::Deserializer;
use serde::{
    de::{self, Visitor},
    Deserialize, Serialize,
};
use std::collections::HashMap;

#[derive(Deserialize)]
pub struct ConfigHashes {
    pub result: ConfigResponse,
}
#[derive(Deserialize)]
pub struct GetChannelResponse {
    pub result: Channel,
}

#[derive(Deserialize)]
pub struct Channel {
    pub channel_id: String,
}

#[derive(Deserialize)]
pub struct ChatChannel {
    pub result: ChatResponse,
}

#[derive(Deserialize)]
pub struct ChatResponse {
    pub channel_id: String,
    pub messages: Vec<Message>,
}

#[derive(Deserialize)]
pub struct Message {
    pub profile_id: String,
    pub clean_text: String,
    #[serde(rename = "NameBit")]
    pub name: Name,
    pub guild_place: i32,
    pub guild_name: String,
    pub redeem: String,
    pub id: i32,
    pub summary_power: i32,
    pub league: i32,
    #[serde(with = "mightyparty_timestamp")]
    pub posted_at: DateTime<Utc>,
    pub role: String,
    pub text: String,
    pub guild_icon: String,
}

#[derive(Deserialize)]
pub struct Name {
    #[serde(rename = "isSavable")]
    pub is_savable: bool,
    #[serde(rename = "Name")]
    pub name: String,
    #[serde(rename = "ChoosenLanguage")]
    pub chosen_language: String,
    #[serde(rename = "Country")]
    pub country: String,
    #[serde(rename = "CountryIsStatic")]
    pub country_is_static: bool,
    #[serde(rename = "Prefix")]
    pub prefix: String,
    #[serde(rename = "medalValue")]
    pub medal_value: i32,
    #[serde(rename = "TimeZone")]
    pub time_zone: String,
    #[serde(rename = "medalId")]
    pub medal_id: i32,
}

#[derive(Deserialize)]
pub struct ConfigResponse {
    pub chunks: Vec<Vec<String>>,
    pub hashes: HashMap<String, String>,
}
#[derive(Serialize, Deserialize)]
pub struct SetPattern {
    pub id: String,
    pub set_army_power: String,
    pub set_cards_count: String,
    pub set_cards_promote: String,
    pub set_guild_power: String,
    pub set_icon: String,
    pub set_power: String,
    pub set_resource_type: String,
    pub set_resource_value: String,
    pub set_unlock_type: String,
    pub set_unlock_value: String,
}

#[derive(Serialize, Deserialize)]
pub struct PromotePattern {
    pub id: String,
    #[serde(deserialize_with = "quoted_int")]
    pub monster_inner_id: i32,
    pub promote_exp: String,
    pub promote_resource_type: String,
    pub promote_resource_value: String,
    pub promote_souls_value: String,
    pub promote_unlock_type: String,
    pub promote_unlock_value: String,
}

#[derive(Serialize, Deserialize)]
pub struct Reset {
    #[serde(deserialize_with = "quoted_int")]
    pub army_power: i32,
    pub bonus_type: String,
    pub bonus_value: String,
    #[serde(deserialize_with = "quoted_int")]
    pub guild_power: i32,
    #[serde(deserialize_with = "quoted_int")]
    pub monster_id: i32,
    #[serde(deserialize_with = "quoted_int")]
    pub power: i32,
    #[serde(deserialize_with = "quoted_int")]
    pub reset_num: i32,
    pub unlock_type: String,
    #[serde(deserialize_with = "quoted_int")]
    pub unlock_value: i32,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum Race {
    Order,
    Nature,
    Chaos,
}
#[derive(Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum Class {
    Ranged,
    Melee,
    Building,
}
#[derive(Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum Gender {
    Male,
    Female,
    MiddleGender,
}
#[derive(Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum Rarity {
    Common,
    Rare,
    Epic,
    Legendary,
}

#[derive(Serialize, Deserialize)]
pub struct Monster {
    #[serde(deserialize_with = "bool_from_int")]
    pub alpha: bool,
    pub animation_name: String,
    pub appear_effect: String,
    #[serde(deserialize_with = "quoted_int")]
    pub army_power: i32,
    pub arrow_sprite: String,
    #[serde(deserialize_with = "quoted_int")]
    pub attack: i32,
    pub boost_id: String,
    pub boost_level: String,
    #[serde(deserialize_with = "quoted_int")]
    pub boost_price: i32,
    #[serde(deserialize_with = "bool_from_int")]
    pub can_collect: bool,
    pub class: Class,
    pub color: String,
    pub death_effect: String,
    pub death_sound: String,
    pub gender: Gender,
    pub gold_effect: String,
    #[serde(deserialize_with = "quoted_int")]
    pub guild_power: i32,
    #[serde(deserialize_with = "quoted_int")]
    pub hp: i32,
    #[serde(deserialize_with = "quoted_int")]
    pub id: i32,
    pub image: String,
    #[serde(deserialize_with = "quoted_float")]
    pub image_scale: f32,
    #[serde(deserialize_with = "quoted_int")]
    pub inner_id: i32,
    #[serde(deserialize_with = "bool_from_int")]
    pub is_event: bool,
    pub max_power: String,
    pub melee_effect: String,
    pub melee_sound: String,
    #[serde(deserialize_with = "quoted_int")]
    pub power: i32,
    pub promote_pattern: String,
    #[serde(default = "none")]
    pub promote_skill: Option<String>,
    pub promote_type: String,
    #[serde(deserialize_with = "missing_quotes")]
    pub promote_value: String,
    pub race: Race,
    pub ranged_effect: String,
    pub ranged_hit_sound: String,
    pub ranged_sound: String,
    pub rarity: Rarity,
    #[serde(deserialize_with = "quoted_int")]
    pub sacrifice_reward: i32,
    #[serde(deserialize_with = "quoted_int")]
    #[serde(default = "zero")]
    pub sacrifice_souls: i32,
    pub set_cards: String,
    pub set_pattern: String,
    pub set_type: String,
    pub set_value: String,
    #[serde(deserialize_with = "missing_quotes")]
    pub skill_value: String,
    pub skills_id: String,
    pub upgrade_monster_id: String,
    pub upgrade_souls_gives: String,
    pub upgrade_souls_price: String,
    pub voice_appear: String,
    pub voice_damage: String,
    pub voice_death: String,
    pub voice_melee: String,
}
fn zero() -> i32 {
    0
}

fn none() -> Option<String> {
    None
}
struct BoolVisitor;

impl<'de> Visitor<'de> for BoolVisitor {
    type Value = bool;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("a bool")
    }

    fn visit_i64<E>(self, value: i64) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(value != 0)
    }

    fn visit_u64<E>(self, value: u64) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(value != 0)
    }

    fn visit_bool<E>(self, v: bool) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(v)
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        match v.parse::<bool>() {
            Ok(b) => Ok(b),
            Err(_) => match v.parse::<i32>() {
                Ok(i) => Ok(i != 0),
                Err(_) => Ok(false),
            },
        }
    }
}
struct IntegerVisitor;

impl<'de> Visitor<'de> for IntegerVisitor {
    type Value = i32;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("an integer between -2^31 and 2^31")
    }

    fn visit_i64<E>(self, value: i64) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        use std::i32;
        if value >= i64::from(i32::MIN) && value <= i64::from(i32::MAX) {
            Ok(value as i32)
        } else {
            Err(E::custom(format!("i32 out of range: {}", value)))
        }
    }

    fn visit_u64<E>(self, value: u64) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        use std::i32;
        if value <= 2147483647u64 {
            Ok(value as i32)
        } else {
            Err(E::custom(format!("i32 out of range: {}", value)))
        }
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        match v.parse::<i32>() {
            Ok(i) => Ok(i),
            Err(_) => Ok(0),
        }
    }
}
struct FloatVisitor;

impl<'de> Visitor<'de> for FloatVisitor {
    type Value = f32;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("a 32-bit float")
    }

    fn visit_i64<E>(self, value: i64) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(value as f32)
    }

    fn visit_u64<E>(self, value: u64) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(value as f32)
    }

    fn visit_f64<E>(self, value: f64) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        if value >= f64::from(-3.40282347e+38_f32) && value <= f64::from(3.40282347e+38_f32) {
            Ok(value as f32)
        } else {
            Err(E::custom(format!("f32 out of range: {}", value)))
        }
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        match v.parse::<f32>() {
            Ok(i) => Ok(i),
            Err(_) => Ok(0f32),
        }
    }
}

struct StringVisitor;

impl<'de> Visitor<'de> for StringVisitor {
    type Value = String;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("a string")
    }

    fn visit_i64<E>(self, value: i64) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(value.to_string())
    }

    fn visit_u64<E>(self, value: u64) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(value.to_string())
    }

    fn visit_f64<E>(self, value: f64) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(value.to_string())
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(v.to_string())
    }
}

fn bool_from_int<'de, D>(deserializer: D) -> Result<bool, D::Error>
where
    D: Deserializer<'de>,
{
    deserializer.deserialize_any(BoolVisitor)
}

fn quoted_int<'de, D>(deserializer: D) -> Result<i32, D::Error>
where
    D: Deserializer<'de>,
{
    deserializer.deserialize_any(IntegerVisitor)
}

fn quoted_float<'de, D>(deserializer: D) -> Result<f32, D::Error>
where
    D: Deserializer<'de>,
{
    deserializer.deserialize_any(FloatVisitor)
}

fn missing_quotes<'de, D>(deserializer: D) -> Result<String, D::Error>
where
    D: Deserializer<'de>,
{
    deserializer.deserialize_any(StringVisitor)
}

mod mightyparty_timestamp {
    use chrono::{DateTime, TimeZone, Utc};
    use serde::{self, Deserialize, Deserializer};

    const FORMAT: &'static str = "%Y-%m-%dT%H:%M:%S.%f";

    // pub fn serialize<S>(date: &DateTime<Utc>, serializer: S) -> Result<S::Ok, S::Error>
    // where
    //     S: Serializer,
    // {
    //     let s = format!("{}", date.format(FORMAT));
    //     serializer.serialize_str(&s)
    // }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<DateTime<Utc>, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        Utc.datetime_from_str(&s, FORMAT).map_err(serde::de::Error::custom)
    }
}
