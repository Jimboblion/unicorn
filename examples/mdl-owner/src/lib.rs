use unicorn_callable::module::UnicornModule;
use unicorn_callable::{define_module, UnicornCommand, UnicornEvent};

mod allguilds;
mod eject;
mod geterror;
mod send;
mod setavatar;
mod shutdown;
mod sysexec;

#[derive(Default)]
pub struct OwnerModule;

impl UnicornModule for OwnerModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![
            allguilds::GuildDumpCommand::boxed(),
            send::SendCommand::boxed(),
            sysexec::SysExecCommand::boxed(),
            eject::EjectCommand::boxed(),
            geterror::GetErrorCommand::boxed(),
            setavatar::SetAvatarCommand::boxed(),
            shutdown::ShutDownCommand::boxed(),
        ]
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        Vec::new()
    }

    fn name(&self) -> &str {
        "Owner"
    }
}

define_module!(OwnerModule, Default::default);
