use serenity::http::CacheHttp;

use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;

use crate::common::InteractionCore;

const INTERACTION_ICON: &str = "👏";
const INTERACTION_COLOR: u64 = 0xff_cc_4d;

#[derive(Default)]
pub struct HighFiveCommand;

impl HighFiveCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

impl UnicornCommand for HighFiveCommand {
    fn command_name(&self) -> &str {
        "highfive"
    }

    fn category(&self) -> &str {
        "interactions"
    }

    fn example(&self) -> &str {
        "@person"
    }

    fn description(&self) -> &str {
        "Give somebody a high-five cause high-fives are awesome!"
    }

    fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let icore = InteractionCore::new(pld.db.clone(), self.command_name());
        let interaction = icore.get_one();
        let (actor, target) = InteractionCore::get_users(pld);
        let action = match target {
            Some(target) => format!("{} high-fives {}.", actor.name, target.name),
            None => format!("{} self-fives.", actor.name),
        };
        let mut response = interaction.embed(&pld.ctx, INTERACTION_COLOR, INTERACTION_ICON, action);
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response)?;
        Ok(())
    }
}
