pub const FAH_ICON: &str = "https://foldingathome.org/wp-content/uploads/2016/09/cropped-folding-at-home-logo-1.png";
pub const FAH_COLOR: u64 = 0xc0_b5_7f;

#[derive(Debug)]
pub enum FAHError {
    ReqwestError(reqwest::Error),
    SerdeJSONError(serde_json::Error),
}

impl From<reqwest::Error> for FAHError {
    fn from(err: reqwest::Error) -> Self {
        FAHError::ReqwestError(err)
    }
}

impl From<serde_json::Error> for FAHError {
    fn from(err: serde_json::Error) -> Self {
        FAHError::SerdeJSONError(err)
    }
}
