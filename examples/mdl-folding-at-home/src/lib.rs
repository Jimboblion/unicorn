use unicorn_callable::module::UnicornModule;
use unicorn_callable::{define_module, UnicornCommand, UnicornEvent};

use crate::donor::DonorCommand;
use crate::team::TeamCommand;

mod common;
mod donor;
mod team;

#[derive(Default)]
pub struct FoldingAtHomeModule;

impl UnicornModule for FoldingAtHomeModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![DonorCommand::boxed(), TeamCommand::boxed()]
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        Vec::new()
    }

    fn name(&self) -> &str {
        "Folding at Home"
    }
}

define_module!(FoldingAtHomeModule, Default::default);
