use bson::Bson;
use log::error;
use serde::{Deserialize, Serialize};

use crate::change::ResourceChange;
use crate::origins::ResourceOrigins;

pub mod change;
pub mod origin;
pub mod origins;

#[derive(Clone, Default, Debug, Serialize, Deserialize)]
pub struct UnicornResource {
    pub user_id: i64,
    pub current: i64,
    pub total: i64,
    pub ranked: i64,
    pub origins: ResourceOrigins,
    pub expenses: ResourceOrigins,
}

impl UnicornResource {
    pub fn add<F>(&mut self, f: F) -> &mut Self
        where
            F: FnOnce(&mut ResourceChange) -> &mut ResourceChange,
    {
        let mut rsc = ResourceChange::new(String::new());
        f(&mut rsc);
        if rsc.amount > 0 {
            self.current += rsc.amount;
            if rsc.tracked {
                self.total += rsc.amount;
                if rsc.ranked {
                    self.ranked += rsc.amount;
                    if let Some(uid) = rsc.user {
                        self.origins.add_user(uid, rsc.amount);
                    }
                    if let Some(cid) = rsc.channel {
                        self.origins.add_channel(cid, rsc.amount);
                    }
                    if let Some(gid) = rsc.guild {
                        self.origins.add_guild(gid, rsc.amount);
                    }
                }
            }
        }
        self
    }

    pub fn del<F>(&mut self, f: F) -> &mut Self
        where
            F: FnOnce(&mut ResourceChange) -> &mut ResourceChange,
    {
        let mut rsc = ResourceChange::new(String::new());
        f(&mut rsc);
        if rsc.amount > 0 {
            self.current -= rsc.amount;
            if rsc.tracked {
                if let Some(uid) = rsc.user {
                    self.expenses.add_user(uid, rsc.amount);
                }
                if let Some(cid) = rsc.channel {
                    self.expenses.add_channel(cid, rsc.amount);
                }
                if let Some(gid) = rsc.guild {
                    self.expenses.add_guild(gid, rsc.amount);
                }
            }
        }
        self
    }

    pub fn to_bson(&self) -> Option<Bson> {
        match bson::to_bson(self) {
            Ok(bs) => Some(bs),
            Err(why) => {
                error!("Failed encoding resource: {}", why);
                None
            }
        }
    }
}
