#[derive(Clone, Debug)]
pub struct ResourceChange {
    pub amount: i64,
    pub tracked: bool,
    pub ranked: bool,
    pub user: Option<u64>,
    pub channel: Option<u64>,
    pub guild: Option<u64>,
    pub function: String,
}

impl ResourceChange {
    pub fn new(function: String) -> Self {
        Self {
            amount: 0,
            tracked: true,
            ranked: true,
            user: None,
            channel: None,
            guild: None,
            function,
        }
    }
}
