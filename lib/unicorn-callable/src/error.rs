use std::fmt::Formatter;
use std::num::ParseIntError;

#[derive(Debug)]
pub enum CallableError {
    String(String),
    STDIOError(std::io::Error),
    SerenityError(serenity::Error),
    ParseIntError(ParseIntError),
}

impl From<serenity::Error> for CallableError {
    fn from(err: serenity::Error) -> Self {
        CallableError::SerenityError(err)
    }
}

impl From<std::io::Error> for CallableError {
    fn from(err: std::io::Error) -> Self {
        CallableError::STDIOError(err)
    }
}

impl From<String> for CallableError {
    fn from(err: String) -> Self {
        CallableError::String(err)
    }
}

impl From<&str> for CallableError {
    fn from(err: &str) -> Self {
        CallableError::String(err.to_owned())
    }
}

impl From<ParseIntError> for CallableError {
    fn from(err: ParseIntError) -> Self {
        CallableError::ParseIntError(err)
    }
}

impl std::fmt::Display for CallableError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            CallableError::String(err) => err.fmt(f),
            CallableError::STDIOError(err) => err.fmt(f),
            CallableError::SerenityError(err) => err.fmt(f),
            CallableError::ParseIntError(err) => err.fmt(f),
        }
    }
}
