use std::any::Any;

use unicorn_config::builder::ConfigurationBuilder;
use unicorn_database::DatabaseHandler;

use crate::error::CallableError;
use crate::payload::ReactionPayload;

pub trait UnicornReaction: Any + Send + Sync {
    fn execute(&self, pld: &mut ReactionPayload) -> Result<(), CallableError>;
    fn emojis(&self) -> Option<Vec<&str>>;
    fn on_load(&self, _db: &DatabaseHandler, _cfg: &mut ConfigurationBuilder) {}
    fn run(&self, pld: &mut ReactionPayload) -> Result<(), CallableError> {
        self.execute(pld)
    }
}
