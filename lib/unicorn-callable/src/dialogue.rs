use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;
use serenity::model::channel::ReactionType;

use unicorn_embed::UnicornEmbed;
use unicorn_utility::user::Avatar;

use crate::error::CallableError;
use crate::payload::{CommandPayload, ReactionPayload};
pub struct Dialogue {
    command_name: String,
    options: Vec<String>,
    pub title: Option<Box<dyn Fn(&mut CommandPayload) -> String + Sync + Send>>,
    pub footer: Option<Box<dyn Fn(&mut CommandPayload) -> String + Sync + Send>>,
    pub description: Option<Box<dyn Fn(&mut CommandPayload) -> String + Sync + Send>>,
    pub timeout: Box<dyn Fn(&mut CommandPayload) -> String + Sync + Send>,
    pub ok_callback: Option<Box<dyn Fn(&mut ReactionPayload) -> Result<bool, CallableError> + Sync + Send>>,
    pub cancel_callback: Option<Box<dyn Fn(&mut ReactionPayload) -> Result<bool, CallableError> + Sync + Send>>,
    pub number_callback: Option<Box<dyn Fn(&mut ReactionPayload) -> Result<bool, CallableError> + Sync + Send>>,
}

impl Dialogue {
    pub fn options(&self) -> Vec<&str> {
        self.options.iter().map(|s| s.as_str()).collect()
    }

    pub fn show_dialog(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut prompt = CreateMessage::default();
        prompt.embed(|e| {
            e.color(0xF9F9F9);
            if let Some(t) = &self.title {
                e.title((t)(pld));
            }
            if let Some(d) = &self.description {
                e.description((d)(pld));
            }
            if let Some(ftr) = &self.footer {
                e.footer(|f| f.text((ftr)(pld)));
            }
            e.author(|a| {
                a.name(pld.msg.author.name.clone());
                a.icon_url(Avatar::url(pld.msg.author.clone()));
                a
            });
            e
        });

        if let Ok(message) = pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut prompt) {
            let partial_key = self.partial_key(pld.msg.author.id);
            pld.cache
                .set(partial_key.clone(), format!("u:{}-{}", message.id.to_string(), self.options().join(",")));

            let http = pld.ctx.http.clone();
            let channel = pld.msg.channel_id.clone();

            for icon in self.options() {
                message.react(pld.ctx.http(), ReactionType::Unicode(icon.to_string()))?;
            }

            let msg = (self.timeout)(pld);
            let full_key = self.full_key(pld.msg.author.id, message.id);
            let cache = pld.cache.clone();

            pld.timeouts.register(&full_key, 30, move || {
                cache.del(partial_key.clone());
                let mut response = UnicornEmbed::small_embed("🕙", 0x696969, msg.clone());
                let _ = channel.delete_message(http.clone(), message.id);
                let _ = channel.send_message(http.clone(), |_| &mut response);
            });
        }
        Ok(())
    }

    fn cleanup_prompt(&self, pld: &mut ReactionPayload, partial_key: String) {
        let _ = pld.reaction.channel_id.delete_message(pld.ctx.http(), pld.reaction.message_id);
        pld.cache.del(partial_key.clone());
    }

    fn process_callback(
        &self,
        callback_result: std::result::Result<bool, CallableError>,
        pld: &mut ReactionPayload,
        full_key: String,
        partial_key: String,
    ) -> Result<(), CallableError> {
        match callback_result {
            Ok(clean_up) => {
                if clean_up {
                    pld.timeouts.cancel(&full_key);
                    self.cleanup_prompt(pld, partial_key);
                } else {
                    pld.timeouts.reset(&full_key, 30);
                }
                Ok(())
            }
            Err(error) => Err(error),
        }
    }

    pub fn check_reaction(&self, pld: &mut ReactionPayload) -> Result<(), CallableError> {
        let mut result = Ok(());
        if let ReactionType::Unicode(emoji) = pld.reaction.emoji.clone() {
            if let Ok(message) = pld.reaction.message(pld.ctx.http()) {
                if let Ok(user) = pld.reaction.user(pld.ctx.http()) {
                    let full_key = self.full_key(user.id, message.id);
                    let partial_key = self.partial_key(user.id);
                    if self.options.contains(&emoji) {
                        if pld.timeouts.check(&full_key) {
                            match emoji.as_ref() {
                                "✅" => {
                                    if let Some(callback) = &self.ok_callback {
                                        result = self.process_callback((callback)(pld), pld, full_key, partial_key);
                                    }
                                }
                                "❌" => {
                                    if let Some(callback) = &self.cancel_callback {
                                        result = self.process_callback((callback)(pld), pld, full_key, partial_key);
                                    }
                                }
                                _ => {
                                    if self.options.contains(&emoji) {
                                        if let Some(callback) = &self.number_callback {
                                            result = self.process_callback((callback)(pld), pld, full_key, partial_key);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        result
    }

    fn full_key<Id1: Into<u64>, Id2: Into<u64>>(&self, user: Id1, message: Id2) -> String {
        format!("{}:user{}:message{}", self.command_name, user.into(), message.into())
    }
    fn partial_key<Id: Into<u64>>(&self, user: Id) -> String {
        format!("{}:user{}", self.command_name, user.into())
    }

    pub fn is_open<Id: Into<u64>>(&self, pld: &CommandPayload, user: Id) -> bool {
        let partial_key = self.partial_key(user.into());
        pld.cache.get(partial_key) != None
    }

    fn new(command_name: &str) -> Self {
        Self {
            command_name: command_name.to_owned(),
            title: None,
            description: None,
            footer: None,
            timeout: Box::new(|_| "The command timed out".to_owned()),
            ok_callback: None,
            cancel_callback: None,
            number_callback: None,
            options: Vec::new(),
        }
    }

    pub fn ok_cancel(command_name: &str) -> Self {
        let mut me = Self::new(command_name);
        me.options = vec!["✅".to_owned(), "❌".to_owned()];
        me
    }

    pub fn numbered_options(command_name: &str, count: usize) -> Self {
        let mut options = Vec::new();
        let source = vec!["1⃣", "2⃣", "3⃣", "4⃣", "5⃣", "6⃣", "7⃣", "8⃣", "9⃣"];
        for i in 0..count {
            match source.get(i) {
                Some(s) => options.push(s.to_string()),
                None => break,
            }
        }
        let mut me = Self::new(command_name);
        me.options = options;
        me
    }

    pub fn choice(reaction: &ReactionType) -> usize {
        let mut result = 0;
        if let ReactionType::Unicode(emoji) = reaction {
            match emoji.as_ref() {
                "✅" | "1⃣" => result = 1,
                "❌" | "2⃣" => result = 2,
                "3⃣" => result = 3,
                "4⃣" => result = 4,
                "5⃣" => result = 5,
                "6⃣" => result = 6,
                "7⃣" => result = 7,
                "8⃣" => result = 8,
                "9⃣" => result = 9,
                _ => result = 0,
            }
        }
        result
    }
}
