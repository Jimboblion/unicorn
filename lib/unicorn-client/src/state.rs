use std::sync::{Arc, Mutex, MutexGuard, PoisonError};

use log::error;

#[derive(Clone, Default)]
pub struct ClientState {
    pub shards: Arc<Mutex<Vec<u64>>>,
}

impl ClientState {
    fn fail(err: PoisonError<MutexGuard<Vec<u64>>>) {
        error!(
            "Failed locking the client state, gracefully proceeding: {}",
            err
        );
    }

    pub fn ready(&self, total: u64) -> bool {
        match self.shards.lock() {
            Ok(rs) => rs.len() as u64 == total,
            Err(why) => {
                Self::fail(why);
                false
            }
        }
    }

    pub fn count(&self) -> usize {
        match self.shards.lock() {
            Ok(rs) => rs.len(),
            Err(why) => {
                Self::fail(why);
                0
            }
        }
    }

    pub fn add_shard(&self, shard: u64) {
        match self.shards.lock() {
            Ok(mut rs) => {
                if !rs.contains(&shard) {
                    rs.push(shard);
                }
            }
            Err(why) => {
                Self::fail(why);
            }
        }
    }
}
