use log::error;
use serenity::{Client, Error};
use serenity::prelude::ClientError;

use unicorn_config::config::Configuration;

use crate::error::UnicornClientError;
use crate::handler::UnicornHandler;

pub struct UnicornClient {
    cfg: Configuration,
    client: Client,
}

impl UnicornClient {
    pub fn new(mut cfg: Configuration) -> Result<Self, UnicornClientError> {
        let handler = UnicornHandler::new(&mut cfg)?;
        let client = Client::new(&cfg.discord.token, handler)?;
        cfg.describe();
        Ok(Self { cfg: cfg, client })
    }

    pub fn run(&mut self) -> Result<(), UnicornClientError> {
        if &self.cfg.discord.token != "" {
            self.client.start_autosharded()?;
            Ok(())
        } else {
            error!("The token is empty, shutting down.");
            Err(UnicornClientError::SerenityError(Error::Client(
                ClientError::InvalidToken,
            )))
        }
    }
}
