use std::cmp::PartialEq;
use std::collections::HashMap;
use std::io::Read;

use image;
use log::debug;
use num::Num;
use reqwest;

const LOW_BAR: u8 = 10;
const HIGH_BAR: u8 = 245;

#[derive(Debug, Clone, Hash, Eq)]
struct Pixel {
    r: u8,
    g: u8,
    b: u8,
}

impl PartialEq for Pixel {
    fn eq(&self, other: &Self) -> bool {
        let limiter = 5;
        let r_diff = Self::abs_diff(self.r, other.r);
        let g_diff = Self::abs_diff(self.g, other.g);
        let b_diff = Self::abs_diff(self.b, other.b);
        (r_diff <= limiter) && (g_diff <= limiter) && (b_diff <= limiter)
    }
}

impl Pixel {
    pub fn is_blank(&self) -> bool {
        let all_black = (self.r <= LOW_BAR) && (self.g <= LOW_BAR) && (self.b <= LOW_BAR);
        let all_white = (self.r >= HIGH_BAR) && (self.g >= HIGH_BAR) && (self.b >= HIGH_BAR);
        all_black || all_white
    }
    pub fn abs_diff(a: u8, b: u8) -> u8 {
        if a > b {
            a - b
        } else {
            b - a
        }
    }

    pub fn to_number(&self) -> u64 {
        match <u64 as Num>::from_str_radix(&self.to_hex_string(), 16) {
            Ok(val) => {
                debug!("{}", val);
                val
            }
            Err(why) => {
                debug!("{}", why);
                0
            }
        }
    }

    pub fn to_hex_string(&self) -> String {
        let hex_result = format!(
            "{:0>2}{:0>2}{:0>2}",
            format!("{:x}", &self.r),
            format!("{:x}", &self.g),
            format!("{:x}", &self.b)
        );
        debug!("{}", hex_result);
        hex_result
    }

    pub fn compare_pixels(pixels: Vec<Self>) -> HashMap<Self, u64> {
        let mut counters = HashMap::new();
        for pixel in pixels {
            let mut count: u64 = counters.get(&pixel).unwrap_or(&0u64).to_owned();
            count += 1;
            counters.insert(pixel, count);
        }
        counters
    }

    pub fn top_color(pixel_hash: HashMap<Self, u64>) -> Option<Self> {
        let mut top: Option<Self> = None;
        for (pk, pv) in pixel_hash.clone() {
            match top.clone() {
                Some(px) => {
                    let last_val = pixel_hash.get(&px.clone()).unwrap_or(&0u64).to_owned();
                    if pv > last_val {
                        top = Some(pk);
                    }
                }
                None => top = Some(pk),
            }
        }
        top
    }
}

pub struct ImageProcessing;

impl ImageProcessing {
    pub fn dominant(bytes: Vec<u8>) -> u64 {
        debug!("{}", bytes.clone().len());
        match image::load_from_memory(bytes.as_slice()) {
            Ok(img) => {
                let as_rgb = img.to_rgb();
                let raw_pixels = as_rgb.pixels();
                let mut pixels = Vec::new();

                for rpixel in raw_pixels {
                    let pixel = Pixel {
                        r: rpixel.0[0],
                        g: rpixel.0[1],
                        b: rpixel.0[2],
                    };
                    if !pixel.is_blank() {
                        pixels.push(pixel);
                    }
                }
                let compared = Pixel::compare_pixels(pixels);
                let top_pixel = Pixel::top_color(compared);
                match top_pixel {
                    Some(tpx) => tpx.to_number(),
                    None => 0,
                }
            }
            Err(why) => {
                debug!("{}", why);
                0
            }
        }
    }

    pub fn from_url(url: String) -> u64 {
        debug!("{}", url);
        match reqwest::blocking::get(&url.replace(".webp", ".png")) {
            Ok(mut resp) => {
                let mut buffer = Vec::new();
                match resp.read_to_end(&mut buffer) {
                    Ok(_) => Self::dominant(buffer),
                    Err(why) => {
                        debug!("{}", why);
                        0
                    }
                }
            }
            Err(why) => {
                debug!("{}", why);
                0
            }
        }
    }
}
