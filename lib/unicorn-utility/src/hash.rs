use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

use md5;

pub struct UnicornHasher;

impl UnicornHasher {
    pub fn calculate_u64_hash<T: Hash>(entry: &T) -> u64 {
        let mut hasher = DefaultHasher::new();
        entry.hash(&mut hasher);
        hasher.finish()
    }

    pub fn calculate_md5_hash(entry: Vec<u8>) -> String {
        let digest = md5::compute(entry);
        format!("{:x}", digest)
    }
}
