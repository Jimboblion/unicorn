use serenity::client::Context;
use serenity::http::CacheHttp;
use serenity::model::channel::Message;
use serenity::model::guild::Role;

pub struct UnicornShorts;

impl UnicornShorts {
    pub fn get_roles(ctx: &Context, msg: &Message) -> Vec<Role> {
        let mut roles = Vec::<Role>::new();
        if let Some(gld) = msg.guild(&ctx.cache) {
            let guild = gld.read();
            for (_, role) in guild.roles.clone() {
                if let Ok(res) = msg.author.has_role(ctx.http(), guild.id, &role) {
                    if res {
                        roles.push(role);
                    }
                }
            }
        };
        roles
    }
}
