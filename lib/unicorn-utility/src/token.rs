use hex;
use rand::Rng;

pub struct UnicornToken;

impl UnicornToken {
    pub fn hex(len: usize) -> String {
        let mut rng = rand::thread_rng();
        let bytes = (0..len).map(|_| rng.gen_range(0u8, 16u8)).collect::<Vec<u8>>();
        hex::encode(bytes)
    }
}
