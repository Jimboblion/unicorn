use std::collections::HashMap;
use std::ops::DerefMut;
use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc, Mutex,
};
use std::thread::{sleep, Builder, JoinHandle};
use std::time::{Duration, Instant};

use threadpool::ThreadPool;

pub struct TimerController {
    interval: Arc<u64>,
    workers: Arc<usize>,
    timers: Arc<Mutex<HashMap<String, Timer>>>,
    running: Arc<AtomicBool>,
    thread: Option<JoinHandle<()>>,
}

impl TimerController {
    pub fn new(interval: u64, workers: usize) -> Self {
        Self {
            interval: Arc::new(interval),
            workers: Arc::new(workers),
            timers: Arc::new(Mutex::new(HashMap::new())),
            running: Arc::new(AtomicBool::new(false)),
            thread: None,
        }
    }

    pub fn start(&mut self) -> Result<(), std::io::Error> {
        self.running.store(true, Ordering::Relaxed);
        let running = self.running.clone();
        let worker_count = self.workers.clone();
        let interval = self.interval.clone();
        let timers = self.timers.clone();
        match Builder::new().name("timer".to_string()).spawn(move || {
            let pool = ThreadPool::new(*worker_count);
            while running.load(Ordering::Relaxed) {
                Self::run(timers.clone(), &pool);
                sleep(Duration::from_millis(*interval));
            }
        }) {
            Ok(j) => {
                self.thread = Some(j);
                Ok(())
            }
            Err(why) => Err(why),
        }
    }

    fn run(timers: Arc<Mutex<HashMap<String, Timer>>>, pool: &ThreadPool) {
        let timers_copy;
        if let Ok(mut timers_for_copy) = timers.lock() {
            timers_copy = timers_for_copy.deref_mut().clone();
        } else {
            return;
        }
        for (key, timer) in timers_copy {
            if timer.expires < Instant::now() {
                if let Ok(mut timers) = timers.lock() {
                    timers.deref_mut().remove(&key);
                }
                pool.execute(move || (timer.callback)());
            }
        }
    }

    pub fn stop(self) -> std::thread::Result<()> {
        self.running.store(false, Ordering::Relaxed);
        if let Some(t) = self.thread {
            t.join()
        } else {
            Ok(())
        }
    }

    pub fn register<F: 'static>(&self, key: &str, duration: u64, callback: F)
    where
        F: Fn() + Send + Sync,
    {
        let now = Instant::now() + Duration::from_secs(duration);
        let timer = Timer::new(key.to_owned(), now, callback);
        if let Ok(mut timers) = self.timers.lock() {
            timers.deref_mut().insert(key.to_owned(), timer);
        }
    }

    pub fn cancel(&self, key: &str) -> bool {
        if let Ok(mut timers) = self.timers.lock() {
            match timers.deref_mut().remove(key) {
                Some(_) => true,
                None => false,
            }
        } else {
            false
        }
    }

    pub fn check(&self, key: &str) -> bool {
        if let Ok(mut timers) = self.timers.lock() {
            timers.deref_mut().contains_key(key)
        } else {
            false
        }
    }

    pub fn reset(&self, key: &str, duration: u64) {
        if let Ok(mut timers) = self.timers.lock() {
            if let Some(timer) = timers.deref_mut().get_mut(key) {
                let now = Instant::now() + Duration::from_secs(duration);
                (*timer).expires = now;
            }
        }
    }
}

#[derive(Clone)]
pub struct Timer {
    key: String,
    expires: Instant,
    callback: Arc<Box<dyn Fn() + Send + Sync>>,
}

impl Timer {
    pub fn new<F: 'static>(key: String, expires: Instant, callback: F) -> Self
    where
        F: Fn() + Send + Sync,
    {
        Self {
            key,
            expires,
            callback: Arc::new(Box::new(callback)),
        }
    }
}
