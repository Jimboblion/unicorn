use r2d2;
use redis;

pub enum UnicornCacheError {
    RedisError(redis::RedisError),
    R2D2Error(r2d2::Error),
}

impl From<redis::RedisError> for UnicornCacheError {
    fn from(err: redis::RedisError) -> Self {
        Self::RedisError(err)
    }
}

impl From<r2d2::Error> for UnicornCacheError {
    fn from(err: r2d2::Error) -> Self {
        Self::R2D2Error(err)
    }
}
