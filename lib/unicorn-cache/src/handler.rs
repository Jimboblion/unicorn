use std::sync::Arc;

use log::{debug, info};

use unicorn_config::cache::CacheConfig;

use crate::memory::MemoryCacher;
use crate::redis::RedisCacher;

#[derive(Debug, Clone)]
pub struct CacheHandler {
    cfg: Arc<CacheConfig>,
    memory: MemoryCacher,
    redis: Option<RedisCacher>,
}

impl CacheHandler {
    pub fn new(cfg: Arc<CacheConfig>) -> Self {
        let memory = MemoryCacher::new(&cfg);
        let redis = if cfg.flavor == "redis" {
            match RedisCacher::new(&cfg) {
                Ok(redis) => {
                    info!("Redis connection established and pooled.");
                    Some(redis)
                }
                Err(_) => None,
            }
        } else {
            None
        };
        Self { cfg, memory, redis }
    }
}

impl CacheHandler {
    pub fn get(&self, key: String) -> Option<String> {
        debug!("Getting from cache with key {}", key);
        match self.cfg.flavor.as_ref() {
            "redis" => match &self.redis {
                Some(redis) => redis.get(key),
                None => self.memory.get(key, false),
            },
            _ => self.memory.get(key, false),
        }
    }

    pub fn set(&self, key: String, value: String) {
        debug!("Writing to cache with key {}", key);
        match self.cfg.flavor.as_ref() {
            "redis" => match &self.redis {
                Some(redis) => redis.set(key, value),
                None => self.memory.set(key, value, false),
            },
            _ => self.memory.set(key, value, false),
        }
    }

    pub fn del(&self, key: String) {
        match self.cfg.flavor.as_ref() {
            "redis" => match &self.redis {
                Some(redis) => redis.del(key),
                None => self.memory.del(key),
            },
            _ => self.memory.del(key),
        }
    }

    pub fn wipe(&mut self) {
        match self.cfg.flavor.as_ref() {
            "redis" => match &self.redis {
                Some(redis) => redis.wipe(),
                None => self.memory.wipe(),
            },
            _ => self.memory.wipe(),
        }
    }
}
