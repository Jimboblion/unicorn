use std::any::Any;

use log::info;
use serde::Deserialize;

use crate::channels::ChannelsConfig;
use crate::common::ConfigLoader;
use crate::currency::CurrencyConfig;

#[derive(Debug, Clone, Deserialize)]
pub struct PreferencesConfig {
    #[serde(default = "PreferencesConfig::default_debug")]
    pub debug: bool,
    #[serde(default = "PreferencesConfig::default_prefix")]
    pub prefix: String,
    #[serde(default = "PreferencesConfig::default_currency")]
    pub currency: CurrencyConfig,
    #[serde(default = "PreferencesConfig::default_channels")]
    pub channels: ChannelsConfig,
    #[serde(default = "PreferencesConfig::default_website")]
    pub website: String,
}

impl ConfigLoader for PreferencesConfig {
    fn file_name() -> String {
        "preferences".to_owned()
    }

    fn default() -> Self {
        Self {
            debug: Self::default_debug(),
            prefix: Self::default_prefix(),
            currency: Self::default_currency(),
            channels: Self::default_channels(),
            website: Self::default_website(),
        }
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn describe(&self) {
        info!(
            "Debugging Mode: {}",
            if self.debug { "Enabled" } else { "Disabled" }
        );
        info!("Default Prefix: {}", &self.prefix);
        self.currency.describe();
        self.channels.describe();
        info!("Website Root: {}", &self.website);
    }
}

impl PreferencesConfig {
    fn default_debug() -> bool {
        match std::env::var(Self::env_name("debug".to_owned())) {
            Ok(var_val) => match var_val.as_ref() {
                "1" | "true" => true,
                _ => false,
            },
            Err(_) => false,
        }
    }

    fn default_prefix() -> String {
        match std::env::var(Self::env_name("prefix".to_owned())) {
            Ok(var_val) => var_val,
            Err(_) => "::>".to_owned(),
        }
    }

    fn default_currency() -> CurrencyConfig {
        CurrencyConfig::default()
    }

    fn default_channels() -> ChannelsConfig {
        ChannelsConfig::default()
    }

    fn default_website() -> String {
        match std::env::var(Self::env_name("website".to_owned())) {
            Ok(var_val) => var_val,
            Err(_) => "https://luciascipher.com/sigma".to_string(),
        }
    }
}
