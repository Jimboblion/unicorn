use std::any::Any;

use log::info;
use serde::Deserialize;

use unicorn_utility::hash::UnicornHasher;

use crate::common::ConfigLoader;

#[derive(Debug, Clone, Deserialize)]
pub struct DatabaseConfig {
    #[serde(default = "DatabaseConfig::default_name")]
    pub name: String,
    #[serde(default = "DatabaseConfig::default_host")]
    pub host: String,
    #[serde(default = "DatabaseConfig::default_port")]
    pub port: u16,
    #[serde(default = "DatabaseConfig::default_auth")]
    pub auth: bool,
    #[serde(default = "DatabaseConfig::default_username")]
    pub username: String,
    #[serde(default = "DatabaseConfig::default_password")]
    pub password: String,
}

impl ConfigLoader for DatabaseConfig {
    fn file_name() -> String {
        "database".to_owned()
    }

    fn default() -> Self {
        Self {
            name: Self::default_name(),
            host: Self::default_host(),
            port: Self::default_port(),
            auth: Self::default_auth(),
            username: Self::default_username(),
            password: Self::default_password(),
        }
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn describe(&self) {
        info!("Name: {}", &self.name);
        info!("Address: {}:{}", &self.host, &self.port);
        info!(
            "Authentication: {}",
            if self.auth { "Enabled" } else { "Disabled" }
        );
        if self.auth {
            info!("Username: {}", &self.username);
            info!(
                "Password Hash: {}",
                UnicornHasher::calculate_u64_hash(&self.password)
            )
        }
    }
}

impl DatabaseConfig {
    fn default_name() -> String {
        std::env::var(Self::env_name("name".to_owned())).unwrap_or_else(|_| "unicorn".to_owned())
    }

    fn default_host() -> String {
        std::env::var(Self::env_name("host".to_owned())).unwrap_or_else(|_| "localhost".to_owned())
    }

    fn default_port() -> u16 {
        match std::env::var(Self::env_name("port".to_owned()))
            .unwrap_or_else(|_| "27017".to_owned())
            .trim()
            .parse::<u16>()
        {
            Ok(prt) => prt,
            Err(_) => 5432u16,
        }
    }

    fn default_auth() -> bool {
        let ath = std::env::var(Self::env_name("port".to_owned()))
            .unwrap_or_else(|_| "0".to_owned())
            .to_lowercase();
        match ath.as_ref() {
            "1" | "true" => true,
            _ => false,
        }
    }

    fn default_username() -> String {
        std::env::var(Self::env_name("username".to_owned())).unwrap_or_else(|_| "admin".to_owned())
    }

    fn default_password() -> String {
        std::env::var(Self::env_name("password".to_owned())).unwrap_or_else(|_| "admin".to_owned())
    }
}
